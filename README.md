# voxl-nodes

This repository generates an ipk package containing Modal AI supported ROS nodes for the VOXL platform.

To use, you don't need to build this repository yourself, just pull the latest package from our downloads page: https://docs.modalai.com/docs/resources/downloads/


## Build Instructions

1) prerequisite: voxl-emulator docker image

Follow the instructions here:

https://gitlab.com/voxl-public/voxl-docker


2) Clone Submodules

Each node is a submodule, and some have their own own submodules too. Clone the main repo and submodules as follows. Best not to do this inside the docker image unless you want all files owned by root....

```bash
~/git$ cd voxl-nodes/
~/git/voxl-nodes$ git submodule update --init --recursive
```

3) Launch the voxl-emulator docker image

```bash
~/git/voxl-nodes$ voxl-docker -i voxl-emulator
```

4) Install dependencies inside the docker. Specify the dependencies should be pulled from either the development (dev) or stable modalai package repos. If building the master branch you should specify `stable`, otherwise `dev`.

```bash
./install_build_deps.sh stable
```

5) Compile inside the docker.

```bash
./build.sh
```

6) Make an ipk package either inside or outside of docker.

```bash
bash-4.3$ exit
~/git/voxl-nodes$ ./make_package.sh

Package Name:  voxl-nodes
version Number:  0.0.1
ar: creating voxl-nodes_0.0.1_8x96.ipk

DONE
```

This will make a new voxl-nodes_x.x.x.ipk file in your working directory. The name and version number came from the ipk/control/control file. If you are updating the package version, edit it there.

### Install IPK on VOXL

You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.


```bash
~/git/voxl-vision-px4$ ./install_on_voxl.sh
pushing voxl-nodes_0.0.1_8x96.ipk to target
searching for ADB device
adb device found
voxl_vio_to_px4_0.0.1_8x96.ipk: 1 file pushed. 2.1 MB/s (51392 bytes in 0.023s)
Removing package voxl-nodes from root...
Installing voxl-nodes (0.0.1) on root.
Configuring voxl-nodes.

Done installing voxl-nodes
```

